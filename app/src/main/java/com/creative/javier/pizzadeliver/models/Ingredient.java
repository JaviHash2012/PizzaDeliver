package com.creative.javier.pizzadeliver.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Ingredient implements Parcelable{
    private boolean half;
    private boolean entire;
    private String ingredient;
    private boolean hasIngrefiend;

    protected Ingredient(Parcel in) {
        half = in.readByte() != 0;
        entire = in.readByte() != 0;
        ingredient = in.readString();
        hasIngrefiend = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (half ? 1 : 0));
        dest.writeByte((byte) (entire ? 1 : 0));
        dest.writeString(ingredient);
        dest.writeByte((byte) (hasIngrefiend ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    public boolean isHalf(){
        return half;
    }

    public void setHalf(boolean half) {
        this.half = half;
    }

    public boolean isEntire() {
        return entire;
    }

    public void setEntire(boolean entire) {
        this.entire = entire;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public boolean isHasIngrefiend() {
        return hasIngrefiend;
    }

    public void setHasIngrefiend(boolean hasIngrefiend) {
        this.hasIngrefiend = hasIngrefiend;
    }

}
