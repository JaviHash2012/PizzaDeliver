package com.creative.javier.pizzadeliver.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class OwnPizza implements Parcelable{
    private String size;
    private List<Ingredient> ingredients;
    private int price;

    public OwnPizza() {
    }

    protected OwnPizza(Parcel in) {
        size = in.readString();
        ingredients = in.createTypedArrayList(Ingredient.CREATOR);
        price = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(size);
        dest.writeTypedList(ingredients);
        dest.writeInt(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OwnPizza> CREATOR = new Creator<OwnPizza>() {
        @Override
        public OwnPizza createFromParcel(Parcel in) {
            return new OwnPizza(in);
        }

        @Override
        public OwnPizza[] newArray(int size) {
            return new OwnPizza[size];
        }
    };

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
