package com.creative.javier.pizzadeliver.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SimplePizza implements Parcelable{
    private String size;
    private String type;
    private int amount;
    private int price;



    public SimplePizza(){}

    protected SimplePizza(Parcel in) {
        size = in.readString();
        type = in.readString();
        amount = in.readInt();
        price = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(size);
        dest.writeString(type);
        dest.writeInt(amount);
        dest.writeInt(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SimplePizza> CREATOR = new Creator<SimplePizza>() {
        @Override
        public SimplePizza createFromParcel(Parcel in) {
            return new SimplePizza(in);
        }

        @Override
        public SimplePizza[] newArray(int size) {
            return new SimplePizza[size];
        }
    };

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
