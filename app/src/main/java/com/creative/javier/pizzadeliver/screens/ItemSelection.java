package com.creative.javier.pizzadeliver.screens;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.creative.javier.pizzadeliver.R;
import com.creative.javier.pizzadeliver.adapters.ItemTemplate;
import com.creative.javier.pizzadeliver.data.Order;
import com.creative.javier.pizzadeliver.models.Appetizer;
import com.creative.javier.pizzadeliver.models.Dessert;
import com.creative.javier.pizzadeliver.models.Drink;
import com.creative.javier.pizzadeliver.models.SimplePizza;

public class ItemSelection extends AppCompatActivity implements View.OnClickListener{
    private SharedPreferences preferences;
    //Buttons to add or remove items
    private ImageButton btnAdd;
    private ImageButton btnRemove;
    private Spinner spSizes;
    //Image for the item
    private ImageView ivItemOrder;
    private Button btnSiguiente;
    //Name of the item
    private TextView tvItemOrden;
    //Item amount
    private TextView tvItemCantidad;
    //If category is pizza enables it
    private TextView tvItemTam;
    //Selected Item
    private ItemTemplate item;
    private int count = 1;
    private String category;
    private String size;
    //Order
    private Order order;
    //Order Items
    private SimplePizza pizza;
    private Drink drink;
    private Dessert dessert;
    private Appetizer appetizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_selection);
        init();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("Categoria",category);
        Log.i("Guardado","Si xD");
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        category = savedInstanceState.getString("Categoria");
    }

    private void init(){
        order = new Order();
        pizza = new SimplePizza();
        drink = new Drink();
        appetizer = new Appetizer();
        dessert = new Dessert();
        preferences = getApplicationContext().getSharedPreferences("Productos",MODE_PRIVATE);
        btnAdd = findViewById(R.id.btnAdd);
        btnRemove = findViewById(R.id.btnRemove);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        ivItemOrder = findViewById(R.id.ivItemOrder);
        tvItemCantidad = findViewById(R.id.tvItemCantidad);
        tvItemTam = findViewById(R.id.tvSizeItem);
        tvItemOrden = findViewById(R.id.tvItemOrden);
        spSizes = findViewById(R.id.spSizes);
        //Get the information item selected
        getItemInfo();
        initSpinner(category);
        //Adding the click listeners
        btnRemove.setEnabled(false);
        btnSiguiente.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        //Setting the image and item selected
        ivItemOrder.setImageResource(item.getImage());
        tvItemOrden.setText(item.getItem());
    }

    //Initialize the spinner
    private void initSpinner(String cat){
        ArrayAdapter<String> adapter;
        //set attributes to order items
        switch (cat){
            case "pizzas":
                adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.pizzaTam));
                spSizes.setAdapter(adapter);
                spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        size = adapterView.getItemAtPosition(i).toString();
                        tvItemTam.setText("Tamaño: "+size);
                        pizza.setType(item.getItem());
                        pizza.setSize(size);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                order.setSimple(true);
                break;
            case "bebidas":
                adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.drinkSizes));
                spSizes.setAdapter(adapter);
                spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        size = adapterView.getItemAtPosition(i).toString();
                        tvItemTam.setText("Tamaño: "+size);
                        drink.setName(item.getItem());
                        drink.setSize(size);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                order.setHasDrink(true);

                break;
            case "entradas":
                adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.orderSizes));
                spSizes.setAdapter(adapter);
                spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        size = adapterView.getItemAtPosition(i).toString();
                        tvItemTam.setText("Tamaño: "+size);
                        appetizer.setName(item.getItem());
                        appetizer.setSize(size);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                order.setHasAppetizer(true);
                break;
            case "postres":
                adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.orderSizes));
                spSizes.setAdapter(adapter);
                spSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        size = adapterView.getItemAtPosition(i).toString();
                        tvItemTam.setText("Tamaño: "+size);
                        dessert.setName(item.getItem());
                        dessert.setSize(size);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                order.setHasDessert(true);
                break;

        }
    }

    //Get information category and Item (image and item)
    private void getItemInfo(){
        item = new ItemTemplate();
        item.setItem(preferences.getString("Item",""));
        item.setImage(preferences.getInt("Image",0));
        category = preferences.getString("Categoria","");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnAdd:
                if (count < 10){
                    count++;
                    btnRemove.setEnabled(true);
                    tvItemCantidad.setText(String.valueOf(count));
                }
                break;

            case R.id.btnRemove:
                if (count > 1){
                    count--;
                    tvItemCantidad.setText(String.valueOf(count));
                }
                else {
                    btnRemove.setEnabled(false);
                }
                break;

            case R.id.btnSiguiente:
                openConfirm();
                break;
        }
    }

    private void openConfirm(){
        Intent next = new Intent(this, Confirm.class);
        switch (category){
            case "pizzas":
                pizza.setAmount(count);
                pizza.setPrice(count*180);
                order.setSimplePizza(pizza);
                break;
            case "bebidas":
                drink.setPrice(count * 35);
                drink.setAmount(count);
                order.setDrink(drink);
                break;
            case "postres":
                dessert.setAmount(count);
                dessert.setPrice(count * 50);
                order.setDessert(dessert);
                break;
            case "entradas":
                appetizer.setAmount(count);
                appetizer.setPrice(count * 70);
                order.setAppetizer(appetizer);
                break;
        }
        next.putExtra("Order",order);
        startActivity(next);

    }
}
