package com.creative.javier.pizzadeliver.screens;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.creative.javier.pizzadeliver.R;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private TextInputEditText edUser;
    private TextInputEditText edPassword;
    private CheckBox session;
    private Button btnLogin;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    //Components Initialization
    private void init(){
        edUser = findViewById(R.id.edUser);
        edPassword = findViewById(R.id.edPassword);
        session = findViewById(R.id.cbSession);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    /*Only Used for Demo*/
    private boolean loginDemo(String user, String password){
        if (validate())
            return user.equals("user@medusa-system.com") && password.equals("12345");
        else
            return false;
    }

    private boolean validate(){
        return (!edUser.getText().toString().equals("") && !edPassword.getText().toString().equals(""));
    }

    @Override
    public void onClick(View view) {
        Intent next = new Intent(this,Main.class);
        String user = edUser.getText().toString();
        String password = edPassword.getText().toString();
        if (view.getId() == R.id.btnLogin){
            //Toast.makeText(this,"Hola"+user+password,Toast.LENGTH_SHORT).show();
            if (loginDemo(user,password)){
                startActivity(next);
                this.finish();
            }
        }
    }
}
