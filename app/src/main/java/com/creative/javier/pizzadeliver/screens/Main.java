package com.creative.javier.pizzadeliver.screens;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.creative.javier.pizzadeliver.R;
import com.creative.javier.pizzadeliver.adapters.ItemTemplate;
import com.creative.javier.pizzadeliver.adapters.RecyclerViewAdapter;
import com.creative.javier.pizzadeliver.data.StaticData;

import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity implements View.OnClickListener{

    private CardView cvPizzas;
    private CardView cvMaker;
    private CardView cvBebidas;
    private CardView cvPostres;
    private CardView cvEntradas;
    private RecyclerView rvProductos;
    private RecyclerView.LayoutManager layoutManager;

    private List<ItemTemplate> dataSet;
    private RecyclerViewAdapter adapter;
    /*
    * These variables are just for demo static data
    * */
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private StaticData data;
    /////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){
        data = new StaticData();
        preferences = getApplicationContext().getSharedPreferences("Productos",MODE_PRIVATE);
        editor = preferences.edit();
        cvBebidas = findViewById(R.id.cvBebidas);
        cvPostres = findViewById(R.id.cvPostres);
        cvEntradas = findViewById(R.id.cvEntradas);
        cvPizzas = findViewById(R.id.cvPizzas);
        cvMaker = findViewById(R.id.cvMaker);
        cvPizzas.setOnClickListener(this);
        cvPostres.setOnClickListener(this);
        cvEntradas.setOnClickListener(this);
        cvBebidas.setOnClickListener(this);
        rvProductos = findViewById(R.id.rvProductos);
        rvProductos.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvProductos.setLayoutManager(layoutManager);
        dataSet = data.fillDemoData("pizzas",this);
        adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnClickItemListener() {
            @Override
            public void onItemClick(ItemTemplate item) {
                openSelectionActivity(item,"pizzas");
            }
        });
        rvProductos.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cvBebidas:
                Toast.makeText(this,"Bebidas",Toast.LENGTH_SHORT).show();
                dataSet.clear();
                dataSet = data.fillDemoData("bebidas",this);
                adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnClickItemListener() {
                    @Override
                    public void onItemClick(ItemTemplate item) {
                        openSelectionActivity(item,"bebidas");
                    }
                });
                rvProductos.setAdapter(adapter);
                break;
            case R.id.cvEntradas:
                Toast.makeText(this,"Entradas",Toast.LENGTH_SHORT).show();
                dataSet.clear();
                dataSet = data.fillDemoData("entradas",this);
                adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnClickItemListener() {
                    @Override
                    public void onItemClick(ItemTemplate item) {
                        openSelectionActivity(item,"entradas");
                    }
                });
                rvProductos.setAdapter(adapter);
                break;
            case R.id.cvMaker:
                Intent next = new Intent(this,PizzaMaker.class);
                startActivity(next);
                break;
            case R.id.cvPizzas:
                Toast.makeText(this,"Pizzas",Toast.LENGTH_SHORT).show();
                dataSet.clear();
                dataSet = data.fillDemoData("pizzas",this);
                adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnClickItemListener() {
                    @Override
                    public void onItemClick(ItemTemplate item) {
                        openSelectionActivity(item,"pizzas");
                    }
                });
                rvProductos.setAdapter(adapter);
                break;
            case R.id.cvPostres:
                Toast.makeText(this,"Postres",Toast.LENGTH_SHORT).show();
                dataSet.clear();
                dataSet = data.fillDemoData("postres",this);
                adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnClickItemListener() {
                    @Override
                    public void onItemClick(ItemTemplate item) {
                        openSelectionActivity(item,"postres");
                    }
                });
                rvProductos.setAdapter(adapter);
                break;
        }
    }

    private void openSelectionActivity(ItemTemplate item, String categoria){
        Intent next = new Intent(this,ItemSelection.class);
        editor.putString("Categoria",categoria);
        editor.putInt("Image",item.getImage());
        editor.putString("Item",item.getItem());
        editor.apply();
        startActivity(next);
    }
}
